#include <fcntl.h>
#include <semaphore.h>
#include <getopt.h>
#include <signal.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

#define DIM(_x) (sizeof(_x)/sizeof((_x)[0]))

struct app
{
    bool         hastok;
    const char  *lockname;
    int          tok;
    int          maxproc;

    sem_t       *sem;

    char        *exec;
    char        *args[256];
};

typedef struct app app_t;

app_t app = { .lockname = "paralock", .maxproc = 3 };

void obtain_token(void)
{
    if (sem_wait(app.sem) != 0)
    {
        perror("Could not wait for semaphore");
        exit(-1);
    }

    app.hastok = true;
}

void release_token(void)
{
    if (app.hastok)
    {
        int val;

        if (sem_post(app.sem) != 0)
        {
            perror("Could not post semaphore");
            exit(-1);
        }

        sem_getvalue(app.sem, &val);

        if (val == app.maxproc)
        {
            puts("UNLINK");
            sem_unlink(app.lockname);
        }
    }
}

void cleanup(void)
{
    release_token();
}

void sighandle(int sig)
{
    switch (sig)
    {
    case SIGTERM: case SIGINT: case SIGPIPE:
        cleanup();
        exit(-1);
        break;
    }
}

void help(int argc, char **argv)
{
    fprintf(stderr, " %s [options] <command> [args ...]\n", argv[0]);

    puts("");
    puts("Run a command in a rate-limited manner.");
    puts("");
    puts("Only a maximum number of processes can be simultaneously");
    puts("associated with any particular lockfile.");
    puts("");
    puts("Options:");
    puts("    -l [file]     Use a specifically named lockfile.");
    puts("    -m [n]        Maximum number of processes.");
}

void do_args(int argc, char **argv)
{
    int opt;
    size_t ind;

    while ((opt = getopt(argc, argv, "l:m:")) != -1)
    {
        switch (opt)
        {
        default:
            help(argc, argv);
            exit(-1);

        case 'l':
            app.lockname = optarg;
            break;
        case 'm':
            app.maxproc = atoi(optarg);
            break;
        }
    }

    if (optind >= argc)
    {
        fprintf(stderr, "Expected command argument.");
        help(argc, argv);
        exit(-1);
    }

    if (app.maxproc <= 0)
        app.maxproc = 1;

    app.exec = argv[optind];

    ind = 0;
    while (optind < argc && ind < DIM(app.args) - 1)
    {
        app.args[ind++] = argv[optind++];
    }

    app.args[ind] = NULL;
}

int main(int argc, char **argv)
{
    pid_t pid;

    do_args(argc, argv);

    signal(SIGTERM, sighandle);
    signal(SIGINT,  sighandle);
    signal(SIGPIPE, sighandle);

    app.sem = sem_open(app.lockname, O_CREAT, 0666, app.maxproc);
    if (!app.sem)
    {
        perror("Could not open semaphore");
        exit(-1);
    }

    obtain_token();

    pid = fork();
    if (pid == 0)
    {
        execvp(app.exec, app.args);
        perror("execv()");
        exit(-1);
    }
    else
    {
        int wstatus;
        do {
            waitpid(pid, &wstatus, 0);
        } while (!(WIFEXITED(wstatus) || WIFSIGNALED(wstatus)));
    }

    cleanup();

    return 0;
}

